﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    private Transform doorPivot;
    private Animator animator;
    
    void Start()
    {
        doorPivot = this.transform.GetChild(0).transform;
        animator = doorPivot.GetComponent<Animator>();
    }


    //public void Open()
    //{
    //    animator.Play("DoorOpen");
    //    Debug.Log("Open");
    //}

    //public void Close()
    //{
    //    animator.Play("DoorClose");
    //    Debug.Log("Close");
    //}
}
