﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillPlate : Waypoint
{
    FoodPlate foodPlate;
    public override event EventHandler OnExecuted;
    public void Start()
    {
        foodPlate = this.GetComponent<FoodPlate>();
    }
    /// <summary>
    /// Calls at the moment of starting of the task execution
    /// </summary>
    /// <param name="walkingUnit"></param>
    public override void Action(WalkingUnit walkingUnit)
    {
        if (!foodPlate.IsFull) foodPlate.FillPlate();
    }

    /// <summary>
    /// Calls every frame when task is active to check if executed 
    /// </summary>
    /// <param name="walkingUnit"></param>
    /// <returns></returns>
    public override bool IsExecuted(WalkingUnit walkingUnit)
    {
        if (foodPlate.IsFull)
        {
            OnExecuted?.Invoke(this,null);
            OnExecuted = null;
            return true;
        }
        else return false;
    }

    public override string ToString()
    {
        return "Наполнить миску " + this.gameObject.name + " по корам " + foodPlate.transform.position;
    }
}
