﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoldingController : MonoBehaviour
{
    public Rigidbody holdingRigid;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    public void UpdateTransform()
    {
        holdingRigid.MovePosition(this.transform.position);
        holdingRigid.MoveRotation(this.transform.rotation);
    }
}
