﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Health : MonoBehaviour
{

    public float value = 10000f;
    public float maxValue;
    public bool IsWarmEnoughToHeal = false;
    public bool IsFeedEnoughToHeal = true;

    public PostProcessVolume vignette;
    public float vignetteStartValue;
    public float vignetteChangeSmoothness;
    // Start is called before the first frame update
    void Start()
    {

    }

    void Update()
    {
        if (!MyFlow.IsGamePaused)
        {
            if (IsWarmEnoughToHeal && IsFeedEnoughToHeal)
            {
                Restore(Time.deltaTime);
            }

            //Vignette
            vignette.weight = Mathf.Lerp(vignette.weight, vignetteStartValue + ((1 - vignetteStartValue) * (1 - (value / maxValue))), vignetteChangeSmoothness);
        }
    }


    private void Restore(float healValue)
    {
        if (healValue < 0)
            healValue = 0;

        value = (value + healValue > maxValue) ? maxValue : value + healValue;
    }

    public void Damage(float damageValue)
    {
        if (damageValue < 0)
            damageValue = 0;
        value = (value - damageValue < 0) ? 0 : value - damageValue;
    }

    // Update is called once per frame
    void OnGUI()
    {
        GUILayout.Space(50);
        GUILayout.Label("Health:" + value.ToString(), GUILayout.Width(200), GUILayout.Height(50));
    }


}
