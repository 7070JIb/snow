﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;
public class WalkingUnit : MonoBehaviour
{
    Transform m_transform;
    NavMeshAgent navAgent;
    Vector3 targetPoint;
    public float minTimeForNewPosition = 5;
    public bool IsWayCycled = true;
    public List<Waypoint> waypoints;
    public List<Waypoint> way1;
    public List<Waypoint> way2;
    public int pathProgress = 0;
    public float alertLevel = 10f;
    private Action state;
    public Queue<Waypoint> emergencyActions = new Queue<Waypoint>();
    public int emergencyActionsCount;
    private Quaternion rotationAtStart;
    private Quaternion targetRotation;
    private bool UpdateRotationFromCode = false;
    public float forceTurnSpeed = 1;
    public float turnSmoothness = 1;
    private float turn;
    private float turnTimer = 0;
    [Space]
    public string currentTask;

    void Start()
    {
        waypoints = way1;
        navAgent = GetComponent<NavMeshAgent>();
        m_transform = this.transform;
        BackToRoute();
    }

    public void Interrupt()
    {
        
    }

    void Update()
    {
        UpdateActions();
    }

    public virtual void UpdateActions()
    {

        emergencyActionsCount = emergencyActions.Count;
        if (emergencyActions.Count > 0)
        {
            if (state != EmergencyTasks)
            {
                StartEmergency();
            }
        }
        else
        {
            if (state != FollowRoute)
            {
                BackToRoute();
            }
        }

        state.Invoke();

        if (UpdateRotationFromCode) UpdateRotation();
    }
    /// <summary>
    /// Emergency command to go somewhere
    /// </summary>
    /// <param name="point"> destination point</param>
    /// <param name="minDistance"> minimal distance to account arrival</param>
    public void MakeGo(Vector3 point, float minDistance)
    {
        GameObject newWP = new GameObject("wp_extra");
        newWP.transform.parent = GameObject.Find("Waypoints").transform;
        newWP.transform.position = point;
        SimpleWaypoint wp = newWP.AddComponent<SimpleWaypoint>();
        wp.Initialize(point, minDistance, true);
        emergencyActions.Enqueue(wp);
    }

    public void BackToRoute()
    {
        state = FollowRoute;
        try
        {
            waypoints[pathProgress].Action(this);
        }
        catch(Exception)
        {
            Debug.LogError("waypoints size = " + waypoints.Count);
            Debug.LogError("pathprogress = " + pathProgress);
        }
    }

    public void StartEmergency()
    {
        state = EmergencyTasks;
        emergencyActions.Peek().Action(this);
    }

    private void EmergencyTasks()
    {
        currentTask = emergencyActions.Peek().ToString();
        if (emergencyActions.Peek().IsExecuted(this))
        {
            Waypoint executedTask = emergencyActions.Dequeue();
            Debug.Log("Выполнился " + executedTask);
            if (executedTask is SimpleWaypoint && (executedTask as SimpleWaypoint).IsExtra)
            {
                Destroy(executedTask.gameObject, 0.1f);
            }
            
            if (emergencyActions.Count > 0) emergencyActions.Peek().Action(this);
        }
    }

    private void FollowRoute()
    {
        currentTask = waypoints[pathProgress].ToString();
        if (waypoints[pathProgress].IsExecuted(this))
        {
            if(++pathProgress == waypoints.Count) pathProgress = 0;
            waypoints[pathProgress].Action(this);
        }
    }

    public void UpdateTargetPoint(Vector3 position)
    {
        navAgent.destination = position;
        targetPoint = position;
    }

    public void SetTargetRotation(Quaternion rotation)
    {
        UpdateRotationFromCode = true;
        rotationAtStart = this.transform.rotation;
        targetRotation = rotation;
        turn = 0;
        turnTimer = 0;
    }
    private void UpdateRotation()
    {
        if(turn < 1)
        {
            turn += ((float)-Math.Pow(turn,2) +turn)* Time.deltaTime * forceTurnSpeed * turnSmoothness + Time.deltaTime * forceTurnSpeed/turnSmoothness;
            turnTimer += Time.deltaTime;
        }
        else
        {
            UpdateRotationFromCode = false;
            turnTimer = 0;
        }
        turn = Mathf.Clamp01(turn);
        this.transform.rotation = Quaternion.Lerp(rotationAtStart, targetRotation, turn);
    }

    void OnGUI()
    {
        GUILayout.Space(40);
        GUILayout.Label("tasks: " + emergencyActions.Count.ToString());
    }

}
