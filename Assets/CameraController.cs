﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

//TEST
public class CameraController : MonoBehaviour
{
    public float cameraSpeed = 10f;
    public float lookSensetivity = 1f;
    HomelessCat.PlayerActions inputs;
    float m_CameraVerticalAngle;
    float m_CameraHorizontalAngle;
    public float cameraRotationLerp;
    Rigidbody rigid;
    Vector3 cameraTargetEuler;
    // Start is called before the first frame update
    void Start()
    {
        inputs = new HomelessCat().Player;
        inputs.Enable();
        m_CameraVerticalAngle = this.transform.rotation.eulerAngles.x;
        m_CameraHorizontalAngle = this.transform.rotation.eulerAngles.y;
        rigid = this.GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update()
    {
        // add vertical inputs to the camera's vertical angle
        m_CameraVerticalAngle += -inputs.Look.ReadValue<Vector2>().y* lookSensetivity * Time.deltaTime * 100f;
        // limit the camera's vertical angle to min/max
        m_CameraVerticalAngle = Mathf.Clamp(m_CameraVerticalAngle, -89f, 89f);
       
        m_CameraHorizontalAngle += inputs.Look.ReadValue<Vector2>().x*lookSensetivity*Time.deltaTime*100f;

        // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
        // this.transform.localEulerAngles = ;
        cameraTargetEuler = new Vector3(m_CameraVerticalAngle, m_CameraHorizontalAngle, 0);
        rigid.MoveRotation(Quaternion.Lerp(this.transform.rotation, Quaternion.Euler(cameraTargetEuler), cameraRotationLerp));
        rigid.MovePosition(this.transform.position + transform.TransformVector(inputs.Move.ReadValue<Vector2>().x, 0, inputs.Move.ReadValue<Vector2>().y) * cameraSpeed / 100);
        //this.transform.position +=;
    }
}
