﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeederNPC : WalkingUnit
{
    public FoodPlate foodPlate;
    public bool HasFeededAt8 = false;
    public bool IsGoingToFeedAt8 = false;
    public bool HasFeededAt18 = false;
    public bool IsGoingToFeedAt18 = false;

    // Update is called once per frame
    void Update()
    {
        base.UpdateActions();
        //If it's time for morning feeding then go and feed
        if ((WeatherAndTime.time > 8 && WeatherAndTime.time < 18) && !HasFeededAt8 && !IsGoingToFeedAt8)
        {
            Debug.Log("time for morning feed");
            HasFeededAt18 = false;

            //go
            MakeGo(foodPlate.transform.position, 2f);
            IsGoingToFeedAt8 = true;

            //task for morning feedeing
            FillPlate fillPlateTask = foodPlate.GetComponent<FillPlate>();
            fillPlateTask.OnExecuted += (sender, e) => { Debug.Log("Наполнил миску"); HasFeededAt8 = true; IsGoingToFeedAt8 = false; };
            //feed
            emergencyActions.Enqueue(fillPlateTask);
        }
        if ((WeatherAndTime.time > 18 || WeatherAndTime.time < 8) && !HasFeededAt18 && !IsGoingToFeedAt18)
        {
            Debug.Log("time for evening feed");
            HasFeededAt8 = false;
            //go
            MakeGo(foodPlate.transform.position, 2f);
            IsGoingToFeedAt18 = true;

            //task for morning feedeing
            FillPlate fillPlateTask = foodPlate.GetComponent<FillPlate>();
            fillPlateTask.OnExecuted += (sender, e) => { HasFeededAt18 = true; IsGoingToFeedAt18 = false; };
            //feed
            emergencyActions.Enqueue(fillPlateTask);
        }
    }
}
