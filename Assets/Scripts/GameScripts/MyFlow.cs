﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
public class MyFlow : MonoBehaviour
{
    public static MyFlow Instance { get; private set; }
    // Start is called before the first frame update
    public CanvasGroup group1;
    public CanvasGroup group12;
    public CanvasGroup group2;
    HomelessCat.PlayerActions inputs;
    public static bool IsGamePaused = false;
    public PostProcessVolume postProcBlur;
    public static DepthOfField cameraBlur;
    public float blurSpeed = 1;
    private Coroutine blurringRoutine;
    void Start()
    {
        Instance = this;
        cameraBlur = postProcBlur.profile.GetSetting<DepthOfField>();
        Screen.SetResolution(1920, 1080, FullScreenMode.FullScreenWindow);
        ResumeGame();
    }
    public static void PauseGame()
    {
        Time.timeScale = 0;
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        IsGamePaused = true;
        cameraBlur.enabled.overrideState = true;

        if (Instance.blurringRoutine != null)
            Instance.StopCoroutine(Instance.blurringRoutine);
        Instance.blurringRoutine = Instance.StartCoroutine(BlurCameraCoroutine());
    }

    public static void ResumeGame()
    {
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        IsGamePaused = false;
        cameraBlur.enabled.overrideState = true;

        if (Instance.blurringRoutine!= null)
            Instance.StopCoroutine(Instance.blurringRoutine);
        Instance.blurringRoutine = Instance.StartCoroutine(CleanCameraCoroutine());
    }

    // Update is called once per frame  
    void Update()
    {

    }

    private static IEnumerator BlurCameraCoroutine()
    {
        while (cameraBlur.focalLength.value < 5f)
        {
            cameraBlur.focalLength.value += Time.unscaledDeltaTime * Instance.blurSpeed;
            yield return null;
        }
        cameraBlur.focalLength.value = 5f;

    }

    private static IEnumerator CleanCameraCoroutine()
    {
        while (cameraBlur.focalLength.value > 2.8f)
        {
            cameraBlur.focalLength.value -= Time.unscaledDeltaTime * Instance.blurSpeed;

            yield return null;
        }
        cameraBlur.focalLength.value = 2.8f;
    }
}
