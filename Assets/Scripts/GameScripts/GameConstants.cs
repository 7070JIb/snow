﻿using UnityEngine;


    public class GameConstants
    {
        // all the constant string used across the game
        public const string k_AxisNameVertical = "Vertical";
        public const string k_AxisNameHorizontal = "Horizontal";
        public const string k_MouseAxisNameVertical = "Mouse Y";
        public const string k_MouseAxisNameHorizontal = "Mouse X";
        public const string k_ButtonNameJump = "Jump";
        public const string k_ButtonNameAction = "Action";
        public const string k_ButtonNameSprint = "Sprint";
        public const string k_ButtonNameCrawl = "Crawl";
        public const string k_ButtonNamePauseMenu = "Pause Menu";
        public const float k_MoveableItemsMagneticForce = 10f;
        public const float k_MoveableItemsThrowForce = 1.5f;
    }