﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Settings Object", menuName = "Game Settings",order =0)]
public class SettingsScrObj : ScriptableObject
{
    public float resolutionX;
    public float resolutionY;

}
