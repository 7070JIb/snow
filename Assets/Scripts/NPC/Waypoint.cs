﻿using System;
using UnityEngine;

public abstract class Waypoint : MonoBehaviour
{
    public abstract event EventHandler OnExecuted;
    public abstract void Action(WalkingUnit walkingUnit);

    public abstract bool IsExecuted(WalkingUnit walkingUnit);
}