﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


[CreateAssetMenu(fileName = "Path", menuName = "NPC Path", order = 0)]
class Path : ScriptableObject
{
    List<Waypoint> waypoints;
}

