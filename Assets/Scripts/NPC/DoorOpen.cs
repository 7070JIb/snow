﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpen : Waypoint
{
    private Transform doorPivot;
    private Animator animator;

    public override event EventHandler OnExecuted;

    public void Start()
    {
        doorPivot = this.transform.GetChild(0);
        animator = doorPivot.GetComponent<Animator>();
    }


    public  override void Action(WalkingUnit walkingUnit)
    {
        animator.Play("DoorOpen");
    }

    public override bool IsExecuted(WalkingUnit walkingUnit)
    {
        OnExecuted?.Invoke(this, null);
        OnExecuted = null;

        return true;
    }
    public override string ToString()
    {
        return "Открыть дверь; " + this.gameObject.name + "; коры: " + this.transform.position;
    }

}
