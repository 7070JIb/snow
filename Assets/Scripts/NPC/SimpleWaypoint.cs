﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleWaypoint : Waypoint
{
    public Vector3 position;
    public float minDistance;
    public bool IsExtra = false;

    public override event EventHandler OnExecuted;

    void Start()
    {
        position = this.transform.position;
    }
    public void Initialize(Vector3 position, float minDistance, bool IsExtra = false)
    {
        this.position = position;
        this.minDistance = minDistance;
        this.IsExtra = IsExtra;
    }
    public override void Action(WalkingUnit walkingUnit)
    {
        walkingUnit.UpdateTargetPoint(this.transform.position);
    }

    public override bool IsExecuted(WalkingUnit walkingUnit)
    {
        if ((position - walkingUnit.transform.position).magnitude < minDistance)
        {
            OnExecuted?.Invoke(this, null);
            OnExecuted = null;

            return true;
        }
        return false;
    }

    public override string ToString()
    {
        return "Дойти до вейпоинта; " + this.gameObject.name + "; коры: " + position;
    }
}
