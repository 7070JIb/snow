﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorClose : Waypoint
{
    private Transform doorPivot;
    private Animator animator;

    public override event EventHandler OnExecuted;

    public void Start()
    {
        doorPivot = this.transform.GetChild(0);
        animator = doorPivot.GetComponent<Animator>();
    }

    public override void Action(WalkingUnit walkingUnit)
    {
        animator.Play("DoorClose");
    }

    public override bool IsExecuted(WalkingUnit walkingUnit)
    {
        OnExecuted?.Invoke(this, null);
        OnExecuted = null;

        return true;
    }

    public override string ToString()
    {
        return "Закрыть дверь; " + this.gameObject.name + "; коры: " + this.transform.position;
    }

}
