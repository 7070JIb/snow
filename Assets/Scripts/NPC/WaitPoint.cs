﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitPoint : Waypoint
{
    private float startTime;
    public float waitTime;

    public override event EventHandler OnExecuted;

    public override void Action(WalkingUnit walkingUnit)
    {
        startTime = Time.time;
        walkingUnit.SetTargetRotation(this.transform.rotation);
    }

    public override bool IsExecuted(WalkingUnit walkingUnit)
    {
        if (Time.time - startTime >= waitTime)
        {
            OnExecuted?.Invoke(this, null);
            return true;
        }
        return false;
    }

    public override string ToString()
    {
        return "Подождать; " + this.gameObject.name + "; коры: " + this.transform.position;
    }
}
