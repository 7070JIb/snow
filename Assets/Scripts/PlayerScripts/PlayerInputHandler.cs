﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;

    public class PlayerInputHandler : MonoBehaviour
    {
        HomelessCat.PlayerActions myInputs;

        [Tooltip("Sensitivity multiplier for moving the camera around")]
        [Range(0.01f, 1)]
        public float lookSensitivity = 1f;
        [Tooltip("Additional sensitivity multiplier for WebGL")]
        public float webglLookSensitivityMultiplier = 0.25f;
        [Tooltip("Limit to consider an input when using a trigger on a controller")]
        public float triggerAxisThreshold = 0.4f;
        [Tooltip("Used to flip the vertical input axis")]
        public bool invertYAxis = false;
        [Tooltip("Used to flip the horizontal input axis")]
        public bool invertXAxis = false;
        private bool IsSprintHeld = false;
        private bool IsThirdActionHeld = false;
        PlayerCharacterController m_PlayerCharacterController;
        Vector3 move;

        //TEST
        bool isPaused = false;
        private void Start()
        {
            myInputs = new HomelessCat().Player;
            m_PlayerCharacterController = GetComponent<PlayerCharacterController>();


            //Sprint
            myInputs.SprintPressed.performed += _ =>
                IsSprintHeld = true;
            myInputs.SprintReleased.performed += _ => 
                IsSprintHeld = false;

            //Third Action (mid mouse button)
            myInputs.ThirdActionButtonPressed.performed += _ =>
                IsThirdActionHeld = true;
            myInputs.ThirdActionbuttonReleased.performed += _ =>
                IsThirdActionHeld = false;

            //Pause
            myInputs.PauseMenu.performed += _ =>
            {
                if (MyFlow.IsGamePaused)
                    MyFlow.ResumeGame();
                else
                    MyFlow.PauseGame();
            };


            myInputs.Enable();

        }

        

        public bool CanProcessInput()
        {
            return Cursor.lockState == CursorLockMode.Locked;
        }

        public Vector3 GetMoveInput()
        {
            if (CanProcessInput())
            {
                Vector3 move = new Vector3(myInputs.Move.ReadValue<Vector2>().x, 0f, myInputs.Move.ReadValue<Vector2>().y);
                // constrain move input to a maximum magnitude of 1, otherwise diagonal movement might exceed the max move speed defined
                move = Vector3.ClampMagnitude(move, 1);
                return move;
            }

            return Vector3.zero;
        }

        public float GetLookInputsHorizontal()
        {
            if (CanProcessInput())
                return ((invertXAxis) ? -myInputs.Look.ReadValue<Vector2>().x : myInputs.Look.ReadValue<Vector2>().x) * lookSensitivity;
            else return 0;
        }

        public float GetLookInputsVertical()
        {
            if (CanProcessInput())
                return ((invertYAxis) ? -myInputs.Look.ReadValue<Vector2>().y : myInputs.Look.ReadValue<Vector2>().y) * lookSensitivity;
            else return 0;
        }

        public bool GetJumpInputDown()
        {
            if (CanProcessInput())
            {
                return myInputs.JumpPressed.triggered;
            }

            return false;
        }

        public bool GetSprintInputHeld()
        {
            if (CanProcessInput())
            {
                return IsSprintHeld;
            }
            return false;
        }

        public bool GetCrawlInputDown()
        {
            if (CanProcessInput())
            {
                return myInputs.Crawl.triggered;
            }
            return false;
        }

        public bool GetPrimaryButtonDown()
        {
            if (CanProcessInput())
            {
                return myInputs.PrimaryActionButtonPressed.triggered;
            }
            return false;
        }
        public bool GetSecondaryButtonDown()
        {
            if (CanProcessInput())
            {
                return myInputs.SecondaryActionButtonPressed.triggered;
            }
            return false;
        }
        public bool GetThirdActionButton()
        {
            if(CanProcessInput())
            {
                return IsThirdActionHeld;
            }
            return false;
        }

    }