﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Interaction : MonoBehaviour
{
    public Camera playerCamera;
    public float interactionMaxDistance = 10f;
    public InteractionPaw interactionPaw;
    public PlayerInputHandler m_Input;
    public IHighlightable prevHighlightedInteractiveItem;
    private IHighlightable itemToInteractWith;
    private Vector3 screenCenter;
    public Transform holdingItemTransform;
    public MoveableObject heldObject;
    public AudioClip meowClip;
    private AudioSource audioSource;

    private void Start()
    {
        screenCenter = new Vector3(Screen.width / 2, Screen.height / 2, 0);
        audioSource = this.GetComponent<AudioSource>();
        audioSource.clip = meowClip;
    }

    private void Update()
    {
        //Определить предмет для взаимодействия
        //Если держим какой-то предмет, то выбирается он, иначе - объект, на который наводимся.
        itemToInteractWith = (heldObject != null) ? heldObject : CheckInteraction();
    }
    private IHighlightable CheckInteraction()
    {
        //Если игрок навелся на объект слоя InteractiveObject и у объекта доступен интерфейс IHighlightable,
        //тогда он по необходимости подсвечивается, и возвращается.

        if ((Physics.Raycast(playerCamera.ScreenPointToRay(screenCenter), out RaycastHit hit, interactionMaxDistance, LayerMask.GetMask("InteractiveObject")))
        && (TryFindHighlightableInParent(hit.transform, out IHighlightable hitItem)))
        {
            interactionPaw.UpdatePawTransform(hit);

            if (hitItem != prevHighlightedInteractiveItem || hitItem.IsHighlighted == false)
            {
                HighlightNewItem(hitItem, ref prevHighlightedInteractiveItem);
            }

            return hitItem;
        }

        //Если игрок не навелся на интерактивный объект, старый подсвеченный объект обесцвечивается, если это нужно.
        HighlightNewItem(null, ref prevHighlightedInteractiveItem);
        return null;
    }
    private bool TryFindHighlightableInParent(Transform obj, out IHighlightable interactiveItem)
    {
        Transform parent = obj.transform;
        do
        {
            if (parent.TryGetComponent(out interactiveItem))
            {
                return true;
            }
            parent = parent.parent;
        }
        while (parent != null);
        interactiveItem = null;
        return false;
    }

    public void TryInteract(bool PrimaryAction)
    {
        if (itemToInteractWith != null)
        {
            if (PrimaryAction)
            {
                itemToInteractWith.PrimaryAction(this.gameObject);
            }
            else
            {
                itemToInteractWith.SecondaryAction(this.gameObject);
            }
        }
    }

    public void HighlightNewItem(IHighlightable newHitItem, ref IHighlightable prevHitItem)
    {
        if (newHitItem == null && prevHitItem != null)
        {
            interactionPaw.Hide();
        }
        else
        if (prevHitItem == null && newHitItem != null)
        {
            interactionPaw.Show();
        }

        if (prevHitItem != null && ((MonoBehaviour)prevHitItem)!=null)
        {
                ((((((((((((prevHitItem)))))))))))).StopHighlightingMaterial();
        }

        newHitItem?.HighlightMaterial();
        prevHitItem = newHitItem;
    }
}

