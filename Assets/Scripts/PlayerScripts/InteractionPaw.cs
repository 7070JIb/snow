﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class InteractionPaw : MonoBehaviour
    {
        private Transform m_transform;
        public Animator animator;
        void Start()
        {
            animator = this.GetComponent<Animator>();

            m_transform = this.transform;
            this.gameObject.SetActive(false);
        }

        public void UpdatePawTransform(RaycastHit hit)
        {
            m_transform.position = hit.point;
            m_transform.rotation.SetLookRotation(hit.normal);
        }
        //Animation Event
        public void OnHidePawAnimatonEnded()
        {
            this.gameObject.SetActive(false);
        }
        public void Show()
        {
            this.gameObject.SetActive(true);
            if (animator.GetCurrentAnimatorStateInfo(0).IsName("pawDisappearanceAnim"))
            {
                animator.Play("Active", 0);
            }

        }
        public void Hide()
        {
            animator.Play("pawDisappearanceAnim", 0);
        }
    }

