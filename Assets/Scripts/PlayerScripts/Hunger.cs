﻿using System;
using UnityEngine;


    public class Hunger: MonoBehaviour
    {
        public float value = 500f;
        public float maxHunger = 1000f;
        private Health health;
        public void Restore(float restoreValue)
        {
            value = (restoreValue + value > maxHunger) ? maxHunger : value + restoreValue;
        }

        void Start()
        {
            health = this.GetComponent<Health>();
        }

        void Update()
        {
            if(value == 0)
            {
                health.Damage(Time.deltaTime);
            }
            else
            {
                value = (value - Time.deltaTime < 0) ? 0 : value - Time.deltaTime;
            }
        }

        void OnGUI()
        {
            GUILayout.Space(200);
            GUILayout.Label("Hunger: " + value + "/" + maxHunger);
        }
    }

