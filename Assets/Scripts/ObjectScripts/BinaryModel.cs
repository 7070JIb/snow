﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BinaryModel",menuName = "BinaryModel ScriptableObject", order = 1)]
public class BinaryModel : ScriptableObject
{
    public GameObject Model1;
    [Space]
    public GameObject Model2;
}