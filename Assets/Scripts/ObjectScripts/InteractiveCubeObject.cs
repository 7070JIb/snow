﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class InteractiveCubeObject : MoveableObject
{
    public override void PrimaryAction(GameObject player)
    {
    }

    public override void HighlightMaterial(bool forced = false)
    {
        if (!_isHighlighted || forced)
        {
            _isHighlighted = true;
            this.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", highlightColor);
        }
    }

    public override void StopHighlightingMaterial(bool forced = false)
    {
        if (_isHighlighted || forced)
        {
            _isHighlighted = false;
            this.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        base.Update();
    }
}
