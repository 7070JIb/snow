﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
    public abstract class PrimitiveInteractiveItem : MonoBehaviour, IHighlightable
    {
        
        public bool _isHighlighted = false;
        [ColorUsage(false,true)]
        public Color highlightColor = Color.yellow;

        public bool IsHighlighted { get => _isHighlighted; set => _isHighlighted = value; }

    public abstract void HighlightMaterial(bool forced = false);

    public abstract void StopHighlightingMaterial(bool forced = false);


        public abstract void PrimaryAction(GameObject player);

        public abstract void SecondaryAction(GameObject player);

    }