﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


public interface IHighlightable
{
    void HighlightMaterial(bool forced = false);
    void StopHighlightingMaterial(bool forced = false);
    void PrimaryAction(GameObject player);
    void SecondaryAction(GameObject player);
    bool IsHighlighted { get; set; }
}

