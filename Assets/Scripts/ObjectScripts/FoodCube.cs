﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class FoodCube : MoveableObject
    {
        public float hungerRecovery = 100f;

        public override void PrimaryAction(GameObject player)
        {
            player.GetComponent<Hunger>().Restore(hungerRecovery);
            player.GetComponent<Interaction>().heldObject = null;
            Debug.Log("destroy fish");
            Destroy(this.gameObject);
        }
        public override void HighlightMaterial(bool forced = false)
        {
            Debug.Log(" Highlight Cube");
            if (!_isHighlighted || forced)
            {
                _isHighlighted = true;
                foreach (var meshRenderer in this.transform.GetChild(0).GetComponentsInChildren<MeshRenderer>())
                    meshRenderer.material.SetColor("_EmissionColor", highlightColor);
            }
        }
        public override void StopHighlightingMaterial(bool forced = false)
        {
            if (_isHighlighted || forced)
            {
                _isHighlighted = false;
                foreach (var meshRenderer in this.transform.GetChild(0).GetComponentsInChildren<MeshRenderer>())
                    meshRenderer.material.SetColor("_EmissionColor", Color.black);
            }
        }
    }
