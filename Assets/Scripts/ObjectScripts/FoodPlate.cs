﻿
using UnityEngine;

public class FoodPlate : BinaryModelInteractive
{
    public bool IsFull;
    public float hungerRestore = 100f;

    public void Start()
    {
        IsFirstModel = true;
    }

    public void EmptyPlate()
    {
        if (IsFull)
        {
           
            ChangeModel();
            IsFull = false;
        }
    }
    public void FillPlate()
    {
        if (!IsFull)
        {
           
            ChangeModel();
            IsFull = true;
        }
    }
    public override void PrimaryAction(GameObject player)
    {
        if (IsFull)
        {
            player.GetComponent<Hunger>().Restore(hungerRestore);
            EmptyPlate();
        }
    }
    public override void SecondaryAction(GameObject player)
    {

    }

    void OnGUI()
    {
        if (GUILayout.Button(new GUIContent("Наполнить миску")))
        {
            FillPlate();
        }
        GUILayout.Space(7);
        if (GUILayout.Button(new GUIContent("Highlight миску")))
        {
            HighlightMaterial(true);
        }
    }
}

