﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BinaryModelInteractive : PrimitiveInteractiveItem
{
    public bool IsFirstModel;
    [SerializeField] private BinaryModel modelSet;
    public override void HighlightMaterial(bool forced = false)
    {
            
        if (!_isHighlighted || forced)
        {
            Debug.Log("highlight");
            _isHighlighted = true;
            foreach (var meshRenderer in this.transform.GetChild(0).GetComponentsInChildren<MeshRenderer>())
            {
                meshRenderer.material.SetColor("_EmissionColor", highlightColor);
            }

        }
    }
    public override void StopHighlightingMaterial(bool forced = false)
    {
        if (_isHighlighted || forced)
        {
            Debug.Log("stop highlight");
            _isHighlighted = false;
            foreach (var meshRenderer in this.transform.GetChild(0).GetComponentsInChildren<MeshRenderer>())
            {
                meshRenderer.material.SetColor("_EmissionColor", Color.black);
            }
        }
    }

    public void ChangeModel()
    {
        Vector3 position = this.transform.GetChild(0).position;
        Vector3 euler = this.transform.GetChild(0).eulerAngles;
        Vector3 scale = this.transform.GetChild(0).localScale;
        GameObject oldModel = this.transform.GetChild(0).gameObject;
        oldModel.transform.transform.parent = null;
        Destroy(oldModel);
        GameObject newModel = Instantiate((IsFirstModel)?modelSet.Model2:modelSet.Model1);
        newModel.transform.parent = this.transform;
        newModel.transform.position = position;
        newModel.transform.eulerAngles = euler;
        newModel.transform.localScale = scale;

        if (_isHighlighted)
        {
            HighlightMaterial(true);
        }
        IsFirstModel = !IsFirstModel;
    }
}
