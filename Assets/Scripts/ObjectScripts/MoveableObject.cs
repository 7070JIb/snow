﻿using System;
using UnityEngine;

    public abstract class MoveableObject : PrimitiveInteractiveItem
    {
        Transform targetTransform;
        Transform modifiedTargetTranform;
        Rigidbody m_Rigidbody;
        public bool IsHeldByPlayer = false;
        private Action itemState;
        public Vector3 anchor;
        public void Start()
        {
            m_Rigidbody = this.GetComponent<Rigidbody>();
            itemState = ToLie;
        }
        public void Update()
        {
            itemState.Invoke();
        }
        private void ToBeCarried()
        {
            //TEST
            modifiedTargetTranform.localPosition = anchor;

            this.transform.position = modifiedTargetTranform.position;

            this.transform.rotation = modifiedTargetTranform.rotation;
        }
        private void ToLie()
        {

        }
        public override void SecondaryAction(GameObject player)
        {
            
            if (IsHeldByPlayer) Release(player);
            else Take(player);
        }

        private void Take(GameObject player)
        {
            targetTransform = player.GetComponent<Interaction>().holdingItemTransform;
            IsHeldByPlayer = true;
            itemState = ToBeCarried;
            player.GetComponent<Interaction>().HighlightNewItem(null, ref player.GetComponent<Interaction>().prevHighlightedInteractiveItem);

            //Create modified Holding Position;
            modifiedTargetTranform = new GameObject("ModifiedHoldingPosition").transform;
            modifiedTargetTranform.parent = targetTransform;
            modifiedTargetTranform.localPosition = anchor;
            modifiedTargetTranform.localRotation = Quaternion.identity;

            player.GetComponent<Interaction>().heldObject = this;

            //layer 10: HeldInteractiveObject
            ChangeLayerTo(10, this.gameObject);
        }

        private void ChangeLayerTo(int layerNum, GameObject go)
        {
            go.layer = layerNum;
            for (int i = 0; i < go.transform.childCount; i++)
            {
                ChangeLayerTo(layerNum, go.transform.GetChild(i).gameObject);
            }
        }

        private void Release(GameObject player)
        {
            IsHeldByPlayer = false;
            itemState = ToLie;

            GameObject.Destroy(modifiedTargetTranform.gameObject);

            player.GetComponent<Interaction>().heldObject = null;

            //layer 9: InteractiveObject
            ChangeLayerTo(9, this.gameObject);

            m_Rigidbody.velocity = player.GetComponent<PlayerCharacterController>().characterVelocity + player.GetComponent<PlayerCharacterController>().cameraTargetGO.transform.forward * GameConstants.k_MoveableItemsThrowForce;
        }
    }

