﻿Shader "Custom/PrimitiveLightingTexture"
{
	Properties
	{
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_Color("Color", Color) = (1,1,1,1)
		_AlphaClip("Alpha clip", Range(0.0, 2.0)) = 0.0
		_LightDistanceEffect("Light distance effect", Float) = 1
		_LightMultiplier("Light multiplier", Float) = 1
	}
		SubShader
			{
			Tags {"Queue" = "Transparent" "RenderType" = "Transparent" }
			LOD 200

				CGPROGRAM

				#pragma surface surf Snowflake alpha:fade
				#pragma target 3.0

				struct SurfaceOutputStandard
		{
			fixed3 Albedo;
			fixed3 Normal;
			half3 Emission;
			half Metallic;
			half Smoothness;
			half Occlusion;
			fixed Alpha;
		};
				sampler2D _MainTex;
				float _LightDistanceEffect;
				float _LightMultiplier;
				half4 _Color;
				float _AlphaClip;
				struct Input
				{
					float2 uv_MainTex;
					float3 worldPos;
				};








				// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
				// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
				// #pragma instancing_options assumeuniformscaling
				UNITY_INSTANCING_BUFFER_START(Props)
					// put more per-instance properties here
				UNITY_INSTANCING_BUFFER_END(Props)

				half4 LightingSnowflake(SurfaceOutputStandard s, half3 lightDir, half atten)
				{
					  half4 c;
					  c.rgb = s.Albedo * _LightColor0.rgb * pow(atten, _LightDistanceEffect) * _LightMultiplier;
					  c.a = s.Alpha;
					  return c;
				}
				void surf(Input IN, inout SurfaceOutputStandard o)
				{
					fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
					o.Albedo = c.rgb;
					o.Alpha = c.a;
				}
				ENDCG
			}
				FallBack "Diffuse"
}
