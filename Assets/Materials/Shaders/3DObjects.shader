﻿Shader "Custom/3Dojects"
{
	Properties
	{
		_Color("Color", Color) = (1,1,1,1)
		_MainTexture("Main Texture", 2D) = "white" {}
		_Brightness("Light Brightness", Float) = 0
	}

		SubShader
	{
		CGPROGRAM

		#pragma surface surf Custom

		sampler2D _MainTexture;
		float _Brightness;
		half4 _Color;
	half4 LightingCustom(SurfaceOutput s, half3 lightDir, half atten) {
		half NdotL = dot(s.Normal, lightDir);
		half4 c;
		c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten) * _Brightness ;
		c.a = s.Alpha;
		return c;
	}

		struct Input
		{
			float2 uv_MainTexture;
		};

	void surf(Input IN, inout SurfaceOutput o)
	{
		o.Albedo.rgb = tex2D(_MainTexture, IN.uv_MainTexture).rgb * _Color.rgb;
	}
	ENDCG
}

	Fallback "Diffuse"

}