﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StreetLight : MonoBehaviour
{
    public GameObject lightCube;
    public Light spotLight;
    public float dayIntensity;
    public float nightIntensity;
    // Start is called before the first frame update
    void Start()
    {
        TurnOn();
        WeatherAndTime.turnOnStreetLights += (sender, e) =>
        {
            if (e == true)
                TurnOn();
            else
                TurnOff();
        };
    }


    // Update is called once per frame
    void Update()
    {
        spotLight.intensity = Mathf.Lerp(nightIntensity, dayIntensity, WeatherAndTime.brightness);
    }
    public void TurnOn()
    {
        lightCube.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", spotLight.color);
        spotLight.enabled = true;
        spotLight.GetComponent<LensFlare>().enabled = true;
    }
    public void TurnOff()
    {
        lightCube.GetComponent<MeshRenderer>().material.SetColor("_EmissionColor", Color.black);
        spotLight.enabled = false;
        spotLight.GetComponent<LensFlare>().enabled = false;
    }
}
