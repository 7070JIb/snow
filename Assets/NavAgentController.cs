﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


    public class NavAgentController : MonoBehaviour
    {
        PlayerInputHandler input;
        public NavMeshAgent agent;
        WalkingUnit agentController;
        void Start()
        {
            input = this.GetComponent<PlayerInputHandler>();
            agentController = agent.GetComponent<WalkingUnit>();
        }


        // Update is called once per frame
        void Update()
        {
            if(input.GetThirdActionButton())
            {
                if(Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0)), out RaycastHit hit, 10000f, LayerMask.GetMask("Walkable")))
                {
                    agentController.MakeGo(hit.point, 0.3f);
                }
            }
        }

        void OnGUI()
        {
            GUILayout.Space(60);
            GUILayout.Label(agent.destination.ToString());
            GUILayout.Space(80);
            GUILayout.Label("thirdbuttonheld: " + input.GetThirdActionButton());
        }
    }
