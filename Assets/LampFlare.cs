﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LampFlare : MonoBehaviour
{
    LensFlare flare;
    public float MaxDistance;
    public float MaxBrightness;
    // Start is called before the first frame update
    void Start()
    {
        flare = this.GetComponent<LensFlare>();
    }

    // Update is called once per frame
    void Update()
    {
        float distance = Mathf.Clamp((this.transform.position - Camera.main.transform.position).magnitude, 0, MaxDistance);

        flare.brightness = (1-(distance / MaxDistance)) * MaxBrightness;
    }
}
