﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Bush : MonoBehaviour
{
    Transform[] bushes;
    Vector3[] bushesStartAngles;
    public float shakingFrequency;
    public float shakingAmplitude;
    void Start()
    {
        bushes = new Transform[transform.childCount];
        bushesStartAngles = new Vector3[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            bushes[i] = transform.GetChild(i);
            bushesStartAngles[i] = bushes[i].localEulerAngles;
        }
    }

    void Update()
    {
        for (int i = 0; i < bushes.Length; i++)
        {

            bushes[i].localEulerAngles = bushesStartAngles[i] + new Vector3(
               Mathf.Sin(Time.time * shakingFrequency),
               Mathf.Sin(Time.time * 1.2f * shakingFrequency),
               Mathf.Sin(Time.time * 1.4f * shakingFrequency)) /** Time.deltaTime*/ * shakingAmplitude;
        }
    }
}
