﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Rendering;
using UnityEngine.Events;
public class WeatherAndTime : MonoBehaviour
{
    public float timeToPassDayInSeconds = 600f;
    public static float time;
    private int hours;
    private int minutes;
    public Light sunLight;
    private Transform sunTransform;
    //InRadians;
    public float sunAngle = Mathf.PI/2f;
    public static float brightness;
    [Space]
    public Color dayLight;
    public float dayLightIntensity;
    public Color nightLight;
    public float nightLightIntensity;
    [Space]
    [ColorUsage(true, true)]
    public Color dayAmbient;
    [ColorUsage(true, true)]
    public Color nightAmbient;
    [Space]
    public Color dayFog;
    public Color nightFog;
    [Space]
    public Material skybox;
    public static event EventHandler<bool> turnOnStreetLights;

    private bool AreStreetLightsOn = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        sunAngle += Mathf.PI  *Time.deltaTime * 2f / timeToPassDayInSeconds;
        brightness = (Mathf.Sin(sunAngle) + 1f)/2f;

        sunLight.color = Color.Lerp(nightLight, dayLight, brightness);
        sunLight.intensity = Mathf.Lerp(nightLightIntensity, dayLightIntensity, brightness);
        
        RenderSettings.ambientSkyColor =
            RenderSettings.ambientGroundColor =
                RenderSettings.ambientEquatorColor = Color.Lerp(nightAmbient, dayAmbient, brightness);
        RenderSettings.fogColor = Color.Lerp(nightFog, dayFog, brightness);
        skybox.SetColor("_Tint", RenderSettings.fogColor);

        time = (((sunAngle + Mathf.PI/2) % (Mathf.PI*2f))/Mathf.PI *6f) * 2f;
        hours = (int)time;
        minutes = (int)((time - hours) * 60f);

        if (time > 17 || time < 7)
        {
            if (!AreStreetLightsOn)
            {
                turnOnStreetLights?.Invoke(this, true);
                AreStreetLightsOn = true;
            }
        }
        else
        {
            if(AreStreetLightsOn)
            {
                turnOnStreetLights.Invoke(this, false);
                AreStreetLightsOn = false;
            }
        }



    }

    private void OnGUI()
    {
        GUILayout.Space(400);
        GUILayout.Label("[400] Time: " + hours + ":"+minutes );
    }
}
