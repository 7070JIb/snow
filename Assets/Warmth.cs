﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class Warmth : MonoBehaviour
{
    public float maxValue = 1000f;
    public float value = 200f;
    public PostProcessVolume vignette;
    public float vignetteChangeSmoothness = 0.3f;
    public float vignetteStartValue = 0.4f;
    public Health health;
    private Collider playerCollider;

    public List<WarmthZone> warmthZones;
    void Start()
    {
        health = this.GetComponent<Health>();
    }

    void Update()
    {
        if (!MyFlow.IsGamePaused)
        {
            //Body Temperature
            float mod = warmthZones.Select(t => t.warm).Max() * Time.deltaTime;
            if (value + mod > maxValue)
            {
                value = maxValue;
            }
            else
            if (value + mod < 0)
            {
                value = 0;
            }
            else
            {
                value += mod;
            }


            //Is able to heal
            if (value >= maxValue / 2)
            {
                health.IsWarmEnoughToHeal = true;
            }
            else
            {
                health.IsWarmEnoughToHeal = false;
            }


            //Cold Damage
            if (value == 0)
            {
                health.Damage(-mod);
            }


            //Vignette

            if (mod >= 0)
            {
                vignette.weight = Mathf.Lerp(vignette.weight, 0, vignetteChangeSmoothness);
            }
            else
            {
                vignette.weight = Mathf.Lerp(vignette.weight, vignetteStartValue + ((1 - vignetteStartValue) * (1 - (value / maxValue))), vignetteChangeSmoothness);
            }
        }

    }

    //Enter warm zone
    void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out WarmthZone warmthZone))
        {
            warmthZones.Add(warmthZone);
        }
    }

    //Exit warm zone
    void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out WarmthZone warmthZone))
        {
            warmthZones.Remove(warmthZone);
        }
    }

    void OnGUI()
    {
        GUILayout.Space(100);
        GUILayout.Label("Warmth: " + value);
    }
}
