﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class WarmthZone : MonoBehaviour
{
    public float warm;
    public WarmthZone(float warmVal)
    {
        warm = warmVal;
    }
}
