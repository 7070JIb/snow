﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class MOver : MonoBehaviour
{
    Rigidbody _rigidbody;
    public PlayerInputHandler inputs;
    public Transform playerHoldingPositionTransform;
    public float speed = 1;
    public float moveSmoothness = 1;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Debug.Log(inputs.GetMoveInput());
            _rigidbody.MovePosition(Vector3.Lerp(this.transform.position, playerHoldingPositionTransform.position, moveSmoothness) );
    }
}
