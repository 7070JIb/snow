﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class WindController : MonoBehaviour
{
    Transform fluger;
    SpriteRenderer spriteRenderer;
    Texture2D texture;
    public float xOrg = 0;
    public float yOrg = 0;
    public float scale = 0;
    float yCoord;
    float timeToChangeDirection = 0f;
    float lastDirectionChangeTime;
    float rotationMod = 1;
    public float windChangeSpeed = 0;
    public Transform wind;
    
    // Start is called before the first frame update
    void Start()
    {
        lastDirectionChangeTime = Time.time;
        timeToChangeDirection = UnityEngine.Random.Range(0.3f, 3);
        fluger = transform.GetChild(0);
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > timeToChangeDirection+ lastDirectionChangeTime)
        {
            timeToChangeDirection = UnityEngine.Random.Range(0.3f, 3);
            lastDirectionChangeTime = Time.time;
            rotationMod = -rotationMod;
        }

        yOrg += Time.deltaTime;
        float sample = Mathf.PerlinNoise(0, yOrg);

        fluger.eulerAngles += new Vector3(0f, rotationMod * sample * windChangeSpeed * Time.deltaTime, 0f);
        wind.eulerAngles = fluger.eulerAngles;
    }
}
