﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class BushQuad : MonoBehaviour
{

    public Texture2D doubleTexture;
    void Start()
    {
        Texture2D texture1 = new Texture2D(doubleTexture.width/2, doubleTexture.height);
        texture1.SetPixels(doubleTexture.GetPixels(0, 0, doubleTexture.width / 2, doubleTexture.height)) ;
        texture1.Apply();
        this.GetComponent<MeshRenderer>().material.SetTexture("_MainTex", texture1);

        Texture2D texture2 = new Texture2D(doubleTexture.width / 2, doubleTexture.height);
        texture2.SetPixels(doubleTexture.GetPixels(doubleTexture.width / 2, 0, doubleTexture.width / 2, doubleTexture.height));
        texture2.Apply();
        this.transform.GetChild(0).GetComponent<MeshRenderer>().material.SetTexture("_MainTex", texture2);
    }

}
